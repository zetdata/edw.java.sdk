package com.zetyun.edw;

import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import net.sf.json.JSONObject;

public class RestAPITest {
	
	
	private static String server="127.0.0.1";
	private static String port="18789";
	private static String lang ="pg";
	private static String database ="datacanvas";
	private static String schema="test_schema";
	

	public static void main(String[] args) throws InterruptedException {
		
		
		/*CreateDB();
		
		Thread.sleep(10000);*/
		
		//database="rest_test_db";
		/*
		CreateSchema();
		
		Thread.sleep(10000);
		
		CreateTable();
		
		Thread.sleep(10000);
		
		CreateUser();
		
		Thread.sleep(10000);
		
		InsertData();
		
		Thread.sleep(10000);
		
		SelectQuery();
		
		Thread.sleep(10000);
		
		DropUser();	
		
		
		Thread.sleep(10000);*/
		
		/*DropTable();
		
		Thread.sleep(10000);*/
		
		/*DropSchema();
		
		Thread.sleep(10000);*/
		
		
		
		DropDatabase();
	}
	

	
	private static void CreateDB() {
		
		String query = "CREATE DATABASE rest_test_db;";
		
		JSONObject result = ExecuteQuery(query);
		
		String code = result.getString("code");
		System.out.println("code is:"+code);		
		
		String id = result.getString("message");
		System.out.println("message is:"+id);	
		
		if(!id.isEmpty()){
			JSONObject status = GetQueryStatus(id);
			System.out.println(status);
		}
	}
	
	private static void CreateSchema() {
		
		String query = "CREATE SCHEMA test_schema;";
		
		JSONObject result = ExecuteQuery(query);
		
		String code = result.getString("code");
		System.out.println("code is:"+code);		
		
		String id = result.getString("message");
		System.out.println("message is:"+id);
		
		if(!id.isEmpty()){
			JSONObject status = GetQueryStatus(id);
			System.out.println(status);
		}
	}

	private static void CreateTable(){

		String query ="CREATE TABLE "+schema+".COMPANY(ID INT PRIMARY KEY NOT NULL,NAME TEXT NOT NULL, AGE INT NOT NULL,ADDRESS CHAR(50),SALARY REAL);";
		JSONObject result = ExecuteQuery(query);
		System.out.println(result);
		
		String code = result.getString("code");
		System.out.println("code is:"+code);	
		
		String id = result.getString("message");
		System.out.println("message is:"+id);	
		
		if(!id.isEmpty()){
			JSONObject status =  GetQueryStatus(id);
			System.out.println(status);
		}
	}
	
	private static void CreateUser(){
		String query ="CREATE USER rest_test_user;";
		JSONObject result = ExecuteQuery(query);
		System.out.println(result);
		
		String code = result.getString("code");
		System.out.println("code is:"+code);	
		
		String id = result.getString("message");
		System.out.println("message is:"+id);	
		
		if(!id.isEmpty()){
			JSONObject status =  GetQueryStatus(id);
			System.out.println(status);
		}
	}
	
	private static void InsertData() {
		String query = "INSERT INTO test_schema.company (id,name,age,address,salary) values (1,'Tommy',20,'home address',200.00);";
		
		JSONObject result = ExecuteQuery(query);
		System.out.println(result);
		
		String code = result.getString("code");
		System.out.println("code is:"+code);	
		
		String id = result.getString("message");
		System.out.println("message is:"+id);	
		
		if(!id.isEmpty()){
			JSONObject status =  GetQueryStatus(id);
			System.out.println(status);
		}
	}

	private static void SelectQuery() {
		String query = "SELECT * FROM test_schema.company;";
		
		JSONObject result = ExecuteQuery(query);
		System.out.println(result);
		
		String code = result.getString("code");
		System.out.println("code is:"+code);	
		
		String id = result.getString("message");
		System.out.println("message is:"+id);	
		
		if(!id.isEmpty()){
			JSONObject status =  GetQueryStatus(id);
			System.out.println(status);
		}
	}

	private  static void DropUser() {
		
		String query ="DROP USER rest_test_user;";
		JSONObject result = ExecuteQuery(query);
		System.out.println(result);
		
		String code = result.getString("code");
		System.out.println("code is:"+code);	
		
		String id = result.getString("message");
		System.out.println("message is:"+id);	
		
		if(!id.isEmpty()){
			JSONObject status =  GetQueryStatus(id);
			System.out.println(status);
		}
	}

	private  static void DropTable() {
		String query ="DROP TABLE "+schema+".company;";		
		
		JSONObject result = ExecuteQuery(query);
		System.out.println(result);
		
		String code = result.getString("code");
		System.out.println("code is:"+code);	
		
		String id = result.getString("message");
		System.out.println("message is:"+id);	
		
		if(!id.isEmpty()){
			JSONObject status =  GetQueryStatus(id);
			System.out.println(status);
		}
	}
	
	private  static void DropSchema() {
		String query ="DROP SCHEMA "+schema+";";		
		
		JSONObject result = ExecuteQuery(query);
		System.out.println(result);
		
		String code = result.getString("code");
		System.out.println("code is:"+code);	
		
		String id = result.getString("message");
		System.out.println("message is:"+id);	
		
		if(!id.isEmpty()){
			JSONObject status =  GetQueryStatus(id);
			System.out.println(status);
		}
	}
	
	private  static void DropDatabase() {
		String query ="DROP DATABASE rest_test_db;";
		
		JSONObject result = ExecuteQuery(query);
		System.out.println(result);
		
		String code = result.getString("code");
		System.out.println("code is:"+code);	
		
		String id = result.getString("message");
		System.out.println("message is:"+id);	
		
		if(!id.isEmpty()){
			JSONObject status =  GetQueryStatus(id);
			System.out.println(status);
		}
	}
	
	

	private static JSONObject ExecuteQuery(String query) {
		
		RestAPIManager restAPIMgr = new RestAPIManager(server,port);
		
		JSONObject result = null;
		
		try {
			result = restAPIMgr.ExecuteQuery(lang, database, query);
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

	private static JSONObject GetQueryStatus(String id) {
		RestAPIManager restAPIMgr = new RestAPIManager(server,port);
		
		JSONObject result = null;
		
		try {
			result = restAPIMgr.GetQueryStatus(id);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

}
