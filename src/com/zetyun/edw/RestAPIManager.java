package com.zetyun.edw;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpEntity;  
import org.apache.http.HttpResponse;  
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;  
import org.apache.http.entity.StringEntity;  
import net.sf.json.JSONObject;  


public class RestAPIManager {

	private String host;
	
	private String port;
	
	public   RestAPIManager() {
		
	}
	
	public RestAPIManager(String host,String port){
		this.host = host;
		this.port = port;
	}
	
	public void setHost(String host){
		this.host = host;
	}
	
	public void setPort(String port){
		this.port = port;
	}
	
	public JSONObject ExecuteQuery(String lang, String db, String query) 
			throws ClientProtocolException, IOException{
		
		DefaultHttpClient httpClient =  new DefaultHttpClient();				
		String url = String.format("http://%s:%s/v1/query", host, port);		
		HttpPost httpPost = new HttpPost(url);
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("lang", lang);
		map.put("db", db);
		map.put("query", query);
		JSONObject json = JSONObject.fromObject(map);
		
		StringEntity entity =   new StringEntity(json.toString());
		httpPost.setEntity(entity);
		HttpResponse httpResponse = httpClient.execute(httpPost);
		HttpEntity httpEntity = httpResponse.getEntity();
		if(httpEntity != null){
			InputStream instream = httpEntity.getContent();
			JSONObject result = ConvertStreamToString(instream);			
			httpPost.abort();
			return result;
		}
		
		return null;
	}
	
	public  JSONObject GetQueryStatus(String statusId) 
			throws ClientProtocolException, IOException{
		
		DefaultHttpClient httpClient =  new DefaultHttpClient();
		String url = String.format("http://%s:%s/v1/status?id=%s", host, port,statusId);	
		HttpGet httpGet = new HttpGet(url);
		
		HttpResponse httpResponse = httpClient.execute(httpGet);
		HttpEntity entity = httpResponse.getEntity();
		if(entity != null){
			InputStream instream = entity.getContent();
			JSONObject array = ConvertStreamToString(instream);			
			httpGet.abort();
			return array;
		}
		
		return null;
	}
	
	private static JSONObject ConvertStreamToString(InputStream is)
			throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		
		while((line = reader.readLine())!= null){
			sb.append(line+"\n");
		}		
		is.close();
		
		JSONObject result = JSONObject.fromObject(sb.toString());
		
		return result;
	}		
}


